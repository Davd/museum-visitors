package com.cgi.visitors;

import java.util.Date;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 * Manage a collection of Visitor objects.
 * It's able to compute the maximum affluence 
 * of visitors and give a time range for it.
 *
 * @author David Rueda
 */
public class VisitorCollection extends LinkedList<Visitor> {
    
    private final Date[] maxAffluenceTimeRange;
    private int maxVisitors;

    public VisitorCollection() {
        this.maxAffluenceTimeRange = new Date[2];
    }

    /**
     * Time range for the maximum affluence of visitors.
     * 
     * @return a 2 Date instances table
     * @throws java.lang.IllegalAccessException
     */
    public Date[] getMaxAffluenceTimeRange() throws IllegalAccessException {
        if (this.maxAffluenceTimeRange[0] == null || this.maxAffluenceTimeRange[1] == null) {
            throw new IllegalAccessException("Affluence of visitors has not been computed yet.");
        }
        return new Date[] { 
            new Date(this.maxAffluenceTimeRange[0].getTime()), 
            new Date(this.maxAffluenceTimeRange[1].getTime()) 
        };
    }

    /**
     * The maximum recorded number of visitors.
     * 
     * @return 
     */
    public int getMaxVisitors() {
        return maxVisitors;
    }
    
    /**
     * Add a visitor and insert it according to its entering time.
     * 
     * @param v
     * @return 
     */
    @Override
    public synchronized boolean add(Visitor v) {
        int newIndex = this.isEmpty() ? 0 : this.size() - 1;
        
        for (ListIterator<Visitor> i = this.listIterator(); i.hasNext();) {
            Visitor visitor = i.next();
            int visitorIndex = i.nextIndex();
            
            if (visitor.getEntry().compareTo(v.getEntry()) < 0) {
                newIndex = visitorIndex;
            }
        }
        
        this.add(newIndex, v);
        return true;
    }
    
    /**
     * Add a date to a linked list of sorted dates (from past to future).
     * 
     * @param dateList
     * @param date 
     */
    private void chronoAdd(LinkedList<Date> dateList, Date date) {
        int newIndex = dateList.isEmpty() ? 0 : dateList.size() - 1;
        
        for (ListIterator<Date> i = dateList.listIterator(); i.hasNext();) {
            Date d = i.next();
            int visitorIndex = i.nextIndex();
            
            if (d.compareTo(date) < 0) {
                newIndex = visitorIndex;
            }
        }
        
        dateList.add(newIndex, date);
    }
    
    /**
     * Compute the maximum recorded visitors.
     */
    public synchronized void computeMaxAffluence() {
        int max = 0, counter = 0;
        Date[] maxDateRange = new Date[2];
        LinkedList<Date> exitTimes = new LinkedList<Date>();
        for (Visitor v : this) {
            counter++;
            chronoAdd(exitTimes, v.getExit());
            int nEltToPoll = 0;
            for (Date exit : exitTimes) {
                if (exit.compareTo(v.getEntry()) < 0) {
                    nEltToPoll++;
                }
            }
            if (nEltToPoll > 0) {
                for (int i = 0; i < nEltToPoll; i++) {
                    exitTimes.poll();
                    counter--;
                }
            }
            
            if (counter > max) {
                max = counter;
                maxDateRange[0] = v.getEntry();
                maxDateRange[1] = exitTimes.getFirst();
            }
        }
        
        this.maxAffluenceTimeRange[0] = maxDateRange[0];
        this.maxAffluenceTimeRange[1] = maxDateRange[1];
        this.maxVisitors = max;
    }
    
    /**
     * Print out the managed visitor's entering dates.
     */
    public synchronized void display() {
        for (Visitor v : this) {
            System.out.println(v.getEntry());
        }
    }
}
