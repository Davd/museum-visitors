package com.cgi.visitors;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Count maximum affluence of visitors from the input file.
 *
 * @author David Rueda
 */
public class App 
{
    public static void main( String[] args )
    {
        try {
            if (args.length == 0) {
                System.out.println("Path to 'visiting times' file missing.");
                return;
            }
            
            File visitingTimesFile = new File(args[0]);
            if (!visitingTimesFile.exists()) {
                System.out.println("The given file does not exist.");
            }
            
            VisitorCollection visitors = new VisitorCollection();
            
            FileReader fr = new FileReader(visitingTimesFile);
            BufferedReader br = new BufferedReader(fr);
            String line;
            while ((line = br.readLine()) != null) {
                String[] times = line.split(",");
                visitors.add(new Visitor(times[0], times[1]));
            }
            
            visitors.computeMaxAffluence();
            
            Date[] maxAffluenceTimeRange = visitors.getMaxAffluenceTimeRange();
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
            System.out.print(dateFormat.format(maxAffluenceTimeRange[0]));
            System.out.print("-" + dateFormat.format(maxAffluenceTimeRange[1]));
            System.out.println(";" + visitors.getMaxVisitors());
        } catch (ParseException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
