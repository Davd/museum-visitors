package com.cgi.visitors;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * A visitor is given a entering and 
 * a leaving time.
 *
 * @author David Rueda
 */
public class Visitor {
    
    private final Date entry;
    private final Date exit;
    
    public Date getEntry() {
        return entry;
    }
    
    public Date getExit() {
        return exit;
    }
    
    public Visitor(String entry, String exit) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        this.entry = format.parse(entry);
        this.exit = format.parse(exit);
    }
}
